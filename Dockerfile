FROM golang:1.17.0-alpine3.14 as build

WORKDIR /src

COPY go.mod .
COPY go.sum .
RUN go mod download

COPY . ./

RUN go build -o /app

FROM alpine:3.14 as runtime

WORKDIR /root/
COPY --from=build /app .
ADD ./jokes/template/* ./template/

CMD ["./app"]