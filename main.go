package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/l0s0s/random_joke/jokes"
	"bitbucket.org/l0s0s/random_joke/storage"
	"go.uber.org/zap"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// var jokesHandler *JokesHandler

func main() {
	config, err := storage.ReadConfig()
	if err != nil {
		log.Fatal("cannot load config:", err)
	}
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatal(err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	client, err := mongo.NewClient(options.Client().ApplyURI(config.MongoURI))
	if err != nil {
		log.Fatal(err)
	}

	database, err := connectToDatabase(ctx, client)
	if err != nil {
		log.Fatal(err)
	}

	jokescollection := database.Collection("jokes")
	userscollection := database.Collection("users")

	jokesHandler := jokes.NewJokesHandler(storage.NewJokesRepository(jokescollection),
		storage.NewUserRepository(userscollection),
		&config,
		logger,
		time.Now().UnixNano())

	go func() {
		oscall := <-c
		log.Printf("system call: %+v", oscall)
		cancel()
	}()

	router := mux.NewRouter()
	router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "http://"+r.Host+"/random", http.StatusFound)
	})
	router.HandleFunc("/new_joke", jokesHandler.NewJoke)
	router.HandleFunc("/random", func(w http.ResponseWriter, r *http.Request) {
		jokesHandler.Seed = time.Now().UnixNano()
		jokesHandler.RandomJokes(w, r)
	})
	router.HandleFunc("/funniest", jokesHandler.Funniest)
	router.HandleFunc("/funniest/{page}", jokesHandler.FunniestPag)
	router.HandleFunc("/register", jokesHandler.Register)
	router.HandleFunc("/all/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "http://"+r.Host+"/all/1", http.StatusFound)
	})
	router.HandleFunc("/all/{page}", jokesHandler.All)
	router.HandleFunc("/{id}", jokesHandler.GetByID)
	router.Use(func(h http.Handler) http.Handler {
		return jokes.Middleware(h, config.TokenKey, logger)
	})

	srv := &http.Server{
		Addr:    ":3000",
		Handler: router,
	}

	go func() {
		log.Println("Server start")
		if err := srv.ListenAndServe(); err != nil {
			log.Fatalf("listen:%+s\n", err)
		}
	}()
	<-ctx.Done()

	ctxShutDown, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	defer func() {
		err = client.Disconnect(ctx)
		if err != nil {
			logger.Fatal("Failed to disconect database", zap.Error(err))
		}
	}()
	err = srv.Shutdown(ctxShutDown)
	if err != nil {
		logger.Fatal("Failed to shutdown server", zap.Error(err))
	}
}

func connectToDatabase(ctx context.Context, client *mongo.Client) (*mongo.Database, error) {
	timeout, cancel := context.WithTimeout(ctx, 15*time.Second)
	defer cancel()

	if err := client.Connect(timeout); err != nil {
		return nil, err
	}

	database := client.Database("random_jokes")
	return database, nil
}
