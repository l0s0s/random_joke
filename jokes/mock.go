// Code generated by moq; DO NOT EDIT.
// github.com/matryer/moq

package jokes

import (
	"context"
	"sync"

	"bitbucket.org/l0s0s/random_joke/storage"

	"go.uber.org/zap"
)

// Ensure, that RepositoryMock does implement Repository.
// If this is not the case, regenerate this file with moq.
var _ Repository = &RepositoryMock{}

// RepositoryMock is a mock implementation of Repository.
//
// 	func TestSomethingThatUsesRepository(t *testing.T) {
//
// 		// make and configure a mocked Repository
// 		mockedRepository := &RepositoryMock{
// 			CountJokesFunc: func(ctx context.Context) (int64, error) {
// 				panic("mock out the CountJokes method")
// 			},
// 			FindJokeByIDFunc: func(ctx context.Context, id string) (*storage.Joke, error) {
// 				panic("mock out the FindJokeByID method")
// 			},
// 			FindJokesFunc: func(ctx context.Context, skip int64, limit int64) ([]storage.Joke, error) {
// 				panic("mock out the FindJokes method")
// 			},
// 			InsertJokeFunc: func(ctx context.Context, joke storage.Joke) error {
// 				panic("mock out the InsertJoke method")
// 			},
// 			IsInJokesFunc: func(ctx context.Context, id string, logger *zap.Logger) bool {
// 				panic("mock out the IsInJokes method")
// 			},
// 		}
//
// 		// use mockedRepository in code that requires Repository
// 		// and then make assertions.
//
// 	}
type RepositoryMock struct {
	// CountJokesFunc mocks the CountJokes method.
	CountJokesFunc func(ctx context.Context) (int64, error)

	// FindJokeByIDFunc mocks the FindJokeByID method.
	FindJokeByIDFunc func(ctx context.Context, id string) (*storage.Joke, error)

	// FindJokesFunc mocks the FindJokes method.
	FindJokesFunc func(ctx context.Context, skip, limit int64) ([]storage.Joke, error)

	// InsertJokeFunc mocks the InsertJoke method.
	InsertJokeFunc func(ctx context.Context, joke storage.Joke) error

	// IsInJokesFunc mocks the IsInJokes method.
	IsInJokesFunc func(ctx context.Context, id string, logger *zap.Logger) bool

	// calls tracks calls to the methods.
	calls struct {
		// CountJokes holds details about calls to the CountJokes method.
		CountJokes []struct {
			// Ctx is the ctx argument value.
			Ctx context.Context
		}
		// FindJokeByID holds details about calls to the FindJokeByID method.
		FindJokeByID []struct {
			// Ctx is the ctx argument value.
			Ctx context.Context
			// ID is the id argument value.
			ID string
		}
		// FindJokes holds details about calls to the FindJokes method.
		FindJokes []struct {
			// Ctx is the ctx argument value.
			Ctx context.Context
			// Skip is the skip argument value.
			Skip int64
			// Limit is the limit argument value.
			Limit int64
		}
		// InsertJoke holds details about calls to the InsertJoke method.
		InsertJoke []struct {
			// Ctx is the ctx argument value.
			Ctx context.Context
			// Joke is the joke argument value.
			Joke storage.Joke
		}
		// IsInJokes holds details about calls to the IsInJokes method.
		IsInJokes []struct {
			// Ctx is the ctx argument value.
			Ctx context.Context
			// ID is the id argument value.
			ID string
			// Logger is the logger argument value.
			Logger *zap.Logger
		}
	}
	lockCountJokes   sync.RWMutex
	lockFindJokeByID sync.RWMutex
	lockFindJokes    sync.RWMutex
	lockInsertJoke   sync.RWMutex
	lockIsInJokes    sync.RWMutex
}

// CountJokes calls CountJokesFunc.
func (mock *RepositoryMock) CountJokes(ctx context.Context) (int64, error) {
	if mock.CountJokesFunc == nil {
		panic("RepositoryMock.CountJokesFunc: method is nil but Repository.CountJokes was just called")
	}
	callInfo := struct {
		Ctx context.Context
	}{
		Ctx: ctx,
	}
	mock.lockCountJokes.Lock()
	mock.calls.CountJokes = append(mock.calls.CountJokes, callInfo)
	mock.lockCountJokes.Unlock()
	return mock.CountJokesFunc(ctx)
}

// CountJokesCalls gets all the calls that were made to CountJokes.
// Check the length with:
//     len(mockedRepository.CountJokesCalls())
func (mock *RepositoryMock) CountJokesCalls() []struct {
	Ctx context.Context
} {
	var calls []struct {
		Ctx context.Context
	}
	mock.lockCountJokes.RLock()
	calls = mock.calls.CountJokes
	mock.lockCountJokes.RUnlock()
	return calls
}

// FindJokeByID calls FindJokeByIDFunc.
func (mock *RepositoryMock) FindJokeByID(ctx context.Context, id string) (*storage.Joke, error) {
	if mock.FindJokeByIDFunc == nil {
		panic("RepositoryMock.FindJokeByIDFunc: method is nil but Repository.FindJokeByID was just called")
	}
	callInfo := struct {
		Ctx context.Context
		ID  string
	}{
		Ctx: ctx,
		ID:  id,
	}
	mock.lockFindJokeByID.Lock()
	mock.calls.FindJokeByID = append(mock.calls.FindJokeByID, callInfo)
	mock.lockFindJokeByID.Unlock()
	return mock.FindJokeByIDFunc(ctx, id)
}

// FindJokeByIDCalls gets all the calls that were made to FindJokeByID.
// Check the length with:
//     len(mockedRepository.FindJokeByIDCalls())
func (mock *RepositoryMock) FindJokeByIDCalls() []struct {
	Ctx context.Context
	ID  string
} {
	var calls []struct {
		Ctx context.Context
		ID  string
	}
	mock.lockFindJokeByID.RLock()
	calls = mock.calls.FindJokeByID
	mock.lockFindJokeByID.RUnlock()
	return calls
}

// FindJokes calls FindJokesFunc.
func (mock *RepositoryMock) FindJokes(ctx context.Context, skip, limit int64) ([]storage.Joke, error) {
	if mock.FindJokesFunc == nil {
		panic("RepositoryMock.FindJokesFunc: method is nil but Repository.FindJokes was just called")
	}
	callInfo := struct {
		Ctx   context.Context
		Skip  int64
		Limit int64
	}{
		Ctx:   ctx,
		Skip:  skip,
		Limit: limit,
	}
	mock.lockFindJokes.Lock()
	mock.calls.FindJokes = append(mock.calls.FindJokes, callInfo)
	mock.lockFindJokes.Unlock()
	return mock.FindJokesFunc(ctx, skip, limit)
}

// FindJokesCalls gets all the calls that were made to FindJokes.
// Check the length with:
//     len(mockedRepository.FindJokesCalls())
func (mock *RepositoryMock) FindJokesCalls() []struct {
	Ctx   context.Context
	Skip  int64
	Limit int64
} {
	var calls []struct {
		Ctx   context.Context
		Skip  int64
		Limit int64
	}
	mock.lockFindJokes.RLock()
	calls = mock.calls.FindJokes
	mock.lockFindJokes.RUnlock()
	return calls
}

// InsertJoke calls InsertJokeFunc.
func (mock *RepositoryMock) InsertJoke(ctx context.Context, joke storage.Joke) error {
	if mock.InsertJokeFunc == nil {
		panic("RepositoryMock.InsertJokeFunc: method is nil but Repository.InsertJoke was just called")
	}
	callInfo := struct {
		Ctx  context.Context
		Joke storage.Joke
	}{
		Ctx:  ctx,
		Joke: joke,
	}
	mock.lockInsertJoke.Lock()
	mock.calls.InsertJoke = append(mock.calls.InsertJoke, callInfo)
	mock.lockInsertJoke.Unlock()
	return mock.InsertJokeFunc(ctx, joke)
}

// InsertJokeCalls gets all the calls that were made to InsertJoke.
// Check the length with:
//     len(mockedRepository.InsertJokeCalls())
func (mock *RepositoryMock) InsertJokeCalls() []struct {
	Ctx  context.Context
	Joke storage.Joke
} {
	var calls []struct {
		Ctx  context.Context
		Joke storage.Joke
	}
	mock.lockInsertJoke.RLock()
	calls = mock.calls.InsertJoke
	mock.lockInsertJoke.RUnlock()
	return calls
}

// IsInJokes calls IsInJokesFunc.
func (mock *RepositoryMock) IsInJokes(ctx context.Context, id string, logger *zap.Logger) bool {
	if mock.IsInJokesFunc == nil {
		panic("RepositoryMock.IsInJokesFunc: method is nil but Repository.IsInJokes was just called")
	}
	callInfo := struct {
		Ctx    context.Context
		ID     string
		Logger *zap.Logger
	}{
		Ctx:    ctx,
		ID:     id,
		Logger: logger,
	}
	mock.lockIsInJokes.Lock()
	mock.calls.IsInJokes = append(mock.calls.IsInJokes, callInfo)
	mock.lockIsInJokes.Unlock()
	return mock.IsInJokesFunc(ctx, id, logger)
}

// IsInJokesCalls gets all the calls that were made to IsInJokes.
// Check the length with:
//     len(mockedRepository.IsInJokesCalls())
func (mock *RepositoryMock) IsInJokesCalls() []struct {
	Ctx    context.Context
	ID     string
	Logger *zap.Logger
} {
	var calls []struct {
		Ctx    context.Context
		ID     string
		Logger *zap.Logger
	}
	mock.lockIsInJokes.RLock()
	calls = mock.calls.IsInJokes
	mock.lockIsInJokes.RUnlock()
	return calls
}

// Ensure, that UserRepositoryMock does implement UserRepository.
// If this is not the case, regenerate this file with moq.
var _ UserRepository = &UserRepositoryMock{}

// UserRepositoryMock is a mock implementation of UserRepository.
//
// 	func TestSomethingThatUsesUserRepository(t *testing.T) {
//
// 		// make and configure a mocked UserRepository
// 		mockedUserRepository := &UserRepositoryMock{
// 			FindUserFunc: func(ctx context.Context, username string) (*storage.Userdata, error) {
// 				panic("mock out the FindUser method")
// 			},
// 			InsertUserFunc: func(ctx context.Context, user storage.Userdata) error {
// 				panic("mock out the InsertUser method")
// 			},
// 			IsInUsersFunc: func(ctx context.Context, username string, logger *zap.Logger) bool {
// 				panic("mock out the IsInUsers method")
// 			},
// 		}
//
// 		// use mockedUserRepository in code that requires UserRepository
// 		// and then make assertions.
//
// 	}
type UserRepositoryMock struct {
	// FindUserFunc mocks the FindUser method.
	FindUserFunc func(ctx context.Context, username string) (*storage.Userdata, error)

	// InsertUserFunc mocks the InsertUser method.
	InsertUserFunc func(ctx context.Context, user storage.Userdata) error

	// IsInUsersFunc mocks the IsInUsers method.
	IsInUsersFunc func(ctx context.Context, username string, logger *zap.Logger) bool

	// calls tracks calls to the methods.
	calls struct {
		// FindUser holds details about calls to the FindUser method.
		FindUser []struct {
			// Ctx is the ctx argument value.
			Ctx context.Context
			// Username is the username argument value.
			Username string
		}
		// InsertUser holds details about calls to the InsertUser method.
		InsertUser []struct {
			// Ctx is the ctx argument value.
			Ctx context.Context
			// User is the user argument value.
			User storage.Userdata
		}
		// IsInUsers holds details about calls to the IsInUsers method.
		IsInUsers []struct {
			// Ctx is the ctx argument value.
			Ctx context.Context
			// Username is the username argument value.
			Username string
			// Logger is the logger argument value.
			Logger *zap.Logger
		}
	}
	lockFindUser   sync.RWMutex
	lockInsertUser sync.RWMutex
	lockIsInUsers  sync.RWMutex
}

// FindUser calls FindUserFunc.
func (mock *UserRepositoryMock) FindUser(ctx context.Context, username string) (*storage.Userdata, error) {
	if mock.FindUserFunc == nil {
		panic("UserRepositoryMock.FindUserFunc: method is nil but UserRepository.FindUser was just called")
	}
	callInfo := struct {
		Ctx      context.Context
		Username string
	}{
		Ctx:      ctx,
		Username: username,
	}
	mock.lockFindUser.Lock()
	mock.calls.FindUser = append(mock.calls.FindUser, callInfo)
	mock.lockFindUser.Unlock()
	return mock.FindUserFunc(ctx, username)
}

// FindUserCalls gets all the calls that were made to FindUser.
// Check the length with:
//     len(mockedUserRepository.FindUserCalls())
func (mock *UserRepositoryMock) FindUserCalls() []struct {
	Ctx      context.Context
	Username string
} {
	var calls []struct {
		Ctx      context.Context
		Username string
	}
	mock.lockFindUser.RLock()
	calls = mock.calls.FindUser
	mock.lockFindUser.RUnlock()
	return calls
}

// InsertUser calls InsertUserFunc.
func (mock *UserRepositoryMock) InsertUser(ctx context.Context, user storage.Userdata) error {
	if mock.InsertUserFunc == nil {
		panic("UserRepositoryMock.InsertUserFunc: method is nil but UserRepository.InsertUser was just called")
	}
	callInfo := struct {
		Ctx  context.Context
		User storage.Userdata
	}{
		Ctx:  ctx,
		User: user,
	}
	mock.lockInsertUser.Lock()
	mock.calls.InsertUser = append(mock.calls.InsertUser, callInfo)
	mock.lockInsertUser.Unlock()
	return mock.InsertUserFunc(ctx, user)
}

// InsertUserCalls gets all the calls that were made to InsertUser.
// Check the length with:
//     len(mockedUserRepository.InsertUserCalls())
func (mock *UserRepositoryMock) InsertUserCalls() []struct {
	Ctx  context.Context
	User storage.Userdata
} {
	var calls []struct {
		Ctx  context.Context
		User storage.Userdata
	}
	mock.lockInsertUser.RLock()
	calls = mock.calls.InsertUser
	mock.lockInsertUser.RUnlock()
	return calls
}

// IsInUsers calls IsInUsersFunc.
func (mock *UserRepositoryMock) IsInUsers(ctx context.Context, username string, logger *zap.Logger) bool {
	if mock.IsInUsersFunc == nil {
		panic("UserRepositoryMock.IsInUsersFunc: method is nil but UserRepository.IsInUsers was just called")
	}
	callInfo := struct {
		Ctx      context.Context
		Username string
		Logger   *zap.Logger
	}{
		Ctx:      ctx,
		Username: username,
		Logger:   logger,
	}
	mock.lockIsInUsers.Lock()
	mock.calls.IsInUsers = append(mock.calls.IsInUsers, callInfo)
	mock.lockIsInUsers.Unlock()
	return mock.IsInUsersFunc(ctx, username, logger)
}

// IsInUsersCalls gets all the calls that were made to IsInUsers.
// Check the length with:
//     len(mockedUserRepository.IsInUsersCalls())
func (mock *UserRepositoryMock) IsInUsersCalls() []struct {
	Ctx      context.Context
	Username string
	Logger   *zap.Logger
} {
	var calls []struct {
		Ctx      context.Context
		Username string
		Logger   *zap.Logger
	}
	mock.lockIsInUsers.RLock()
	calls = mock.calls.IsInUsers
	mock.lockIsInUsers.RUnlock()
	return calls
}
