package jokes

import (
	"context"
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"text/template"

	"bitbucket.org/l0s0s/random_joke/storage"
	"go.uber.org/zap"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
)

var middlewareRequest MiddlewareRequest

// Claims is all jwt claims look like this.
type Claims struct {
	jwt.StandardClaims
	Username string `json:"username"`
}

// MiddlewareRequest is how look request body to add new joke.
type MiddlewareRequest struct {
	Token string `json:"token"`
	Body  string `json:"body"`
	ID    string `json:"id"`
	Score int    `json:"score"`
	Title string `json:"title"`
}

// DataForTemplate is transmitted to templates.
type DataForTemplate struct {
	Domain            string
	RandJokes         [5]storage.Joke
	CurrentPage       int64
	PreviousPage      int
	NextPage          int
	IsLast            bool
	Search            string
	IsNothingToSearch bool
}

// TokenResponse is how look response with new token.
type TokenResponse struct {
	Token string
}

// ErrorResponse is how look response with error.
type ErrorResponse struct {
	Error string
}

// Middleware check token from requests.
func Middleware(h http.Handler, TokenKey string, logger *zap.Logger) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		blacklist := []string{"/register", "/all"}
		isInBlackList := func() bool {
			for _, value := range blacklist {
				if strings.Contains(r.URL.Path, value) {
					return true
				}
			}
			return false
		}()

		isTokenValid := false
		if !isInBlackList {
			err := json.NewDecoder(r.Body).Decode(&middlewareRequest)
			if err != nil {
				logger.Error("Failed to decode request", zap.Error(err))
			}

			isTokenValid, _ = auth(middlewareRequest.Token, TokenKey)
		}

		if isTokenValid || isInBlackList {
			h.ServeHTTP(w, r)
		} else {

			response := map[string]string{"error": "Неправильный токен"}
			err := json.NewEncoder(w).Encode(response)
			if err != nil {
				logger.Error("Failed to encode response to json", zap.Error(err))
			}
		}
	})
}

func auth(userToken, tokenKey string) (bool, error) {
	_, err := jwt.ParseWithClaims(userToken, &Claims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(tokenKey), nil
	})
	if err != nil {
		return false, err
	}

	return true, nil
}

// Repository contain methods which shuld be in Repository struct.
type Repository interface {
	FindJokes(ctx context.Context, skip, limit int64) ([]storage.Joke, error)
	CountJokes(ctx context.Context) (int64, error)
	FindJokeByID(ctx context.Context, id string) (*storage.Joke, error)
	InsertJoke(ctx context.Context, joke storage.Joke) error
	IsInJokes(ctx context.Context, id string, logger *zap.Logger) bool
}

// UserRepository contain methods which shuld be in UserRepository struct.
type UserRepository interface {
	InsertUser(ctx context.Context, user storage.Userdata) error
	FindUser(ctx context.Context, username string) (*storage.Userdata, error)
	IsInUsers(ctx context.Context, username string, logger *zap.Logger) bool
}

// Handler is how look handler.
type Handler struct {
	JokesRepository Repository
	UserRepository  UserRepository
	Config          *storage.Config
	Logger          *zap.Logger
	Seed            int64
}

// NewJokesHandler return specimen of new handler.
func NewJokesHandler(JokesRepository Repository, UserRepository UserRepository,
	config *storage.Config, logger *zap.Logger, seed int64) *Handler {
	return &Handler{
		JokesRepository: JokesRepository,
		UserRepository:  UserRepository,
		Config:          config,
		Seed:            seed,
	}
}

// Register need for add new user and generates new token.
func (h *Handler) Register(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), h.Config.DefaultRequestTimeout)
	defer cancel()

	var userdata storage.Userdata

	err := json.NewDecoder(r.Body).Decode(&userdata)
	if err != nil {
		h.Logger.Error("Failed to decode request", zap.Error(err))
	}
	if userdata.Username == "" || userdata.Password == "" {
		errorResponse := ErrorResponse{
			"Пустые поля",
		}

		err := json.NewEncoder(w).Encode(errorResponse)
		if err != nil {
			h.Logger.Error("Failed to encode response to json", zap.Error(err))
		}
		return
	}

	if h.UserRepository.IsInUsers(ctx, userdata.Username, h.Logger) {
		errorResponse := ErrorResponse{
			"Пользователь с таким именем уже существует",
		}

		err := json.NewEncoder(w).Encode(errorResponse)
		if err != nil {
			h.Logger.Error("Failed to encode response to json", zap.Error(err))
		}

		return
	}

	pwd := sha1.New()
	pwd.Write([]byte(userdata.Password))

	userdata.Password = fmt.Sprintf("%x", pwd.Sum(nil))

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, Claims{
		Username: userdata.Username,
	})

	err = h.UserRepository.InsertUser(ctx, userdata)
	if err != nil {
		h.Logger.Error("Failed to insert user", zap.Error(err))
	}

	tokenString, _ := token.SignedString([]byte(h.Config.TokenKey))
	response := TokenResponse{
		tokenString,
	}

	err = json.NewEncoder(w).Encode(response)
	if err != nil {
		h.Logger.Error("Failed to encode response to json", zap.Error(err))
	}
}

// Funniest is return jokes from the most fun joke to the most boring joke.
func (h *Handler) Funniest(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), h.Config.DefaultRequestTimeout)
	defer cancel()

	jokes, err := h.JokesRepository.FindJokes(ctx, int64(0), int64(0))
	if err != nil {
		h.Logger.Error("Failed to find jokes", zap.Error(err))
	}

	err = json.NewEncoder(w).Encode(jokes)
	if err != nil {
		h.Logger.Error("Failed to encode response to json", zap.Error(err))
	}
}

// FunniestPag is return with pagination jokes from the most fun joke to the most boring joke.
func (h *Handler) FunniestPag(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), h.Config.DefaultRequestTimeout)
	defer cancel()

	page := mux.Vars(r)["page"]

	currentPage, _ := strconv.Atoi(page)

	var pageSize int

	size := r.URL.Query().Get("size")

	if size == "" {
		pageSize = 5
	} else {
		var err error
		pageSize, err = strconv.Atoi(size)
		if err != nil {
			h.Logger.Error("Failed to convert string to int", zap.Error(err))
		}

	}

	len, err := h.JokesRepository.CountJokes(ctx)
	if err != nil {
		h.Logger.Error("Failed to count jokes", zap.Error(err))
	}

	len = len - 1

	var error ErrorResponse

	if int(len)-1 > pageSize {
		error.Error = "Incorrect page size"
	}

	start := (currentPage - 1) * pageSize

	stop := currentPage * pageSize

	limit := len + int64(stop) - len
	skip := len + int64(start) - len

	jokes, err := h.JokesRepository.FindJokes(ctx, limit, skip)
	if err != nil {
		h.Logger.Error("Failed to find jokes", zap.Error(err))
	}

	err = json.NewEncoder(w).Encode(jokes)
	if err != nil {
		h.Logger.Error("Failed to encode response to json", zap.Error(err))
	}
}

// RandomJokes return random jokes.
func (h *Handler) RandomJokes(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), h.Config.DefaultRequestTimeout)
	defer cancel()

	var randJokes [50]storage.Joke

	jokes, err := h.JokesRepository.FindJokes(ctx, int64(0), int64(0))
	if err != nil {
		h.Logger.Error("Failed to find jokes", zap.Error(err))
	}

	rnd := rand.New(rand.NewSource(h.Seed))

	for i := range randJokes {
		index := rnd.Intn(len(jokes) - 1)
		for _, value := range randJokes {
			if value != jokes[index] {
				randJokes[i] = jokes[index]
			}
		}
	}

	quantity := r.URL.Query().Get("quantity")

	if quantity == "" {
		err := json.NewEncoder(w).Encode(randJokes)
		if err != nil {
			h.Logger.Error("Failed to encode response to json", zap.Error(err))
		}
	} else {
		quantityResponse, _ := strconv.ParseInt(quantity, 10, 64)
		err = json.NewEncoder(w).Encode(randJokes[0:quantityResponse])
		if err != nil {
			h.Logger.Error("Failed to encode response to json", zap.Error(err))
		}

	}
}

// GetByID find one joke by id.
func (h *Handler) GetByID(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), h.Config.DefaultRequestTimeout)
	defer cancel()

	id := mux.Vars(r)["id"]

	joke, err := h.JokesRepository.FindJokeByID(ctx, id)
	if err != nil {
		h.Logger.Error("Failed to find joke by ID", zap.Error(err))
	}

	err = json.NewEncoder(w).Encode(joke)
	if err != nil {
		h.Logger.Error("Failed to encode response to json", zap.Error(err))
	}
}

// All is html page with jokes.
func (h *Handler) All(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), h.Config.DefaultRequestTimeout)
	defer cancel()

	search := r.URL.Query().Get("search")
	page := mux.Vars(r)["page"]

	jokes, err := h.JokesRepository.FindJokes(ctx, int64(0), int64(0))
	if err != nil {
		h.Logger.Error("Failed to find jokes", zap.Error(err))
	}

	var templData DataForTemplate

	templData.Domain = "https://" + r.Host + "/"
	templData.Search = search
	templData.CurrentPage, _ = strconv.ParseInt(page, 10, 64)
	templData.PreviousPage = int(templData.CurrentPage - 1)
	templData.NextPage = int(templData.CurrentPage + 1)
	currentJokesStop := (templData.CurrentPage) * 5
	currentJokesStart := (templData.CurrentPage - 1) * 5

	if search != "" {
		searchJokes := h.SearchByText(search, &templData)
		if len(searchJokes) < int(currentJokesStop) {
			currentJokesStop = int64(len(searchJokes))
		}
		for i, value := range searchJokes[currentJokesStart:currentJokesStop] {
			templData.RandJokes[i] = value

			if templData.RandJokes[i] == searchJokes[len(searchJokes)-1] {
				templData.IsLast = true
			}
		}
	} else {

		templData.IsNothingToSearch = false
		for i, value := range jokes[currentJokesStart:currentJokesStop] {
			templData.RandJokes[i] = value
			if templData.RandJokes[i] == jokes[len(jokes)-1] {
				templData.IsLast = true
			}
		}
	}

	templ, err := template.ParseFiles("template/all.html")
	if err != nil {
		h.Logger.Error("Failed to find template", zap.Error(err))
	}

	err = templ.Execute(w, templData)
	if err != nil {
		h.Logger.Error("Failed to execute all.html", zap.Error(err))
	}
}

// SearchByText is search joke by her Title and Body.
func (h *Handler) SearchByText(search string, templData *DataForTemplate) (searchJokes []storage.Joke) {
	ctx, cancel := context.WithTimeout(context.Background(), h.Config.DefaultRequestTimeout)
	defer cancel()

	jokes, err := h.JokesRepository.FindJokes(ctx, int64(0), int64(0))
	if err != nil {
		h.Logger.Error("Failed to find jokes", zap.Error(err))
	}

	for _, value := range jokes {
		isTitleContain := strings.Contains(value.Title, search)
		isBodyContain := strings.Contains(value.Body, search)

		if isTitleContain || isBodyContain {
			searchJokes = append(searchJokes, value)
		}
	}
	if len(searchJokes) == 0 {
		templData.IsNothingToSearch = true

		searchJokes, err = h.JokesRepository.FindJokes(ctx, 0, 50)
		if err != nil {
			h.Logger.Error("Failed to find jokes", zap.Error(err))
		}
	}
	return
}

// NewJoke add new joke.
func (h *Handler) NewJoke(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), h.Config.DefaultRequestTimeout)
	defer cancel()

	if middlewareRequest.Body == "" || middlewareRequest.Title == "" || middlewareRequest.ID == "" {
		w.WriteHeader(http.StatusForbidden)

		return
	}
	if h.JokesRepository.IsInJokes(ctx, middlewareRequest.ID, h.Logger) {
		w.WriteHeader(http.StatusForbidden)

		fmt.Fprintf(w, "Шутка с таким ID существует")
		return
	}

	err := h.JokesRepository.InsertJoke(ctx, storage.Joke{
		Body:  middlewareRequest.Body,
		Title: middlewareRequest.Title,
		ID:    middlewareRequest.ID,
		Score: middlewareRequest.Score,
	})
	if err != nil {
		h.Logger.Error("Failed to insert joke", zap.Error(err))
	}

	w.WriteHeader(http.StatusOK)
}
