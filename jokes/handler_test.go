package jokes_test

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/l0s0s/random_joke/jokes"
	"bitbucket.org/l0s0s/random_joke/storage"
	"go.uber.org/zap"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var mockJokes = []storage.Joke{
	{
		Body:  "Pizza doesn't scream when you put it in the oven .\n\nI'm so sorry.",
		ID:    "5tz4dd",
		Score: 0,
		Title: "What's the difference between a Jew in Nazi Germany and pizza ?",
	},
	{
		Body:  "But its a silly comparison really, its like comparing apples to oranges. ",
		ID:    "5s9jog",
		Score: 39570,
		Title: "Steve jobs would have been a better president than Donald Trump.",
	},
	{
		Body:  "Whoops, wrong sub.",
		ID:    "2qmkf0",
		Score: 30863,
		Title: "TIFU by mixing up by wifes sandwich order at Subway",
	},
}

var funniest = []storage.Joke{
	{
		Body:  "But its a silly comparison really, its like comparing apples to oranges. ",
		ID:    "5s9jog",
		Score: 39570,
		Title: "Steve jobs would have been a better president than Donald Trump.",
	},
	{
		Body:  "Whoops, wrong sub.",
		ID:    "2qmkf0",
		Score: 30863,
		Title: "TIFU by mixing up by wifes sandwich order at Subway",
	},
	{
		Body:  "Pizza doesn't scream when you put it in the oven .\n\nI'm so sorry.",
		ID:    "5tz4dd",
		Score: 0,
		Title: "What's the difference between a Jew in Nazi Germany and pizza ?",
	},
}

func NewTestServer(url string, handlefunc func(http.ResponseWriter, *http.Request), middleware mux.MiddlewareFunc) (ts *httptest.Server) {
	router := mux.NewRouter()
	router.HandleFunc(url, handlefunc)
	if middleware != nil {
		router.Use(middleware)
	}
	ts = httptest.NewServer(router)

	return
}

func TestGetJokeById(t *testing.T) {
	jokesRepo := &jokes.RepositoryMock{}
	jokesRepo.FindJokeByIDFunc = func(ctx context.Context, id string) (*storage.Joke, error) {
		return &mockJokes[0], nil
	}
	logger := zap.NewNop()
	config, err := storage.ReadConfig()
	require.NoError(t, err)
	handler := jokes.NewJokesHandler(jokesRepo, nil, &config, logger, 0)

	ts := NewTestServer("/{id}", handler.GetByID, nil)
	defer ts.Close()
	request, err := http.NewRequestWithContext(context.Background(), http.MethodGet, ts.URL+"/5tz4dd", nil)
	require.NoError(t, err)
	client := http.Client{}
	res, err := client.Do(request)
	require.NoError(t, err)
	if res.Body != nil {
		defer res.Body.Close()
	}

	var got storage.Joke
	err = json.NewDecoder(res.Body).Decode(&got)
	require.NoError(t, err)
	assert.Equal(t, mockJokes[0], got)
}

func FunniestBase(t *testing.T, addr, requestddr string, handlerfunc func(w http.ResponseWriter, r *http.Request)) {
	ts := NewTestServer(addr, handlerfunc, nil)
	defer ts.Close()

	request, err := http.NewRequestWithContext(context.Background(), http.MethodGet, ts.URL+requestddr, nil)
	require.NoError(t, err)
	client := http.Client{}
	res, err := client.Do(request)
	require.NoError(t, err)

	if res.Body != nil {
		defer res.Body.Close()
	}
	var got []storage.Joke
	err = json.NewDecoder(res.Body).Decode(&got)
	require.NoError(t, err)
	assert.Equal(t, funniest, got)
	assert.Equal(t, 3, len(got))
}

func TestFunniest(t *testing.T) {
	jokesRepo := &jokes.RepositoryMock{}
	jokesRepo.FindJokesFunc = func(ctx context.Context, skip, limit int64) ([]storage.Joke, error) {
		return funniest, nil
	}
	logger := zap.NewNop()
	config, err := storage.ReadConfig()
	require.NoError(t, err)
	handler := jokes.NewJokesHandler(jokesRepo, nil, &config, logger, 0)
	FunniestBase(t, "/funniest", "/funniest", handler.Funniest)
}

func TestFunniestPag(t *testing.T) {
	jokesRepo := &jokes.RepositoryMock{}
	jokesRepo.FindJokesFunc = func(ctx context.Context, skip, limit int64) ([]storage.Joke, error) {
		return funniest, nil
	}
	logger := zap.NewNop()

	config, err := storage.ReadConfig()
	require.NoError(t, err)
	handler := jokes.NewJokesHandler(jokesRepo, nil, &config, logger, 0)
	FunniestBase(t, "/funniest/{page}", "/funniest/1?size=3", handler.Funniest)
}

func TestRegister(t *testing.T) {
	usersRepo := &jokes.UserRepositoryMock{}
	usersRepo.InsertUserFunc = func(ctx context.Context, user storage.Userdata) error {
		return nil
	}
	usersRepo.IsInUsersFunc = func(ctx context.Context, username string, logger *zap.Logger) bool {
		return false
	}
	logger := zap.NewNop()
	config, err := storage.ReadConfig()
	require.NoError(t, err)
	handler := jokes.NewJokesHandler(nil, usersRepo, &config, logger, 0)
	ts := NewTestServer("/register", handler.Register, nil)
	defer ts.Close()
	newUser := storage.Userdata{
		Username: "test",
		Password: "test",
	}

	userEncoded, err := json.Marshal(newUser)
	require.NoError(t, err)

	r := bytes.NewReader(userEncoded)
	request, err := http.NewRequestWithContext(context.Background(), http.MethodGet, ts.URL+"/register", r)
	require.NoError(t, err)
	client := http.Client{}
	res, err := client.Do(request)
	require.NoError(t, err)
	if res.Body != nil {
		defer res.Body.Close()
	}
	var got jokes.TokenResponse
	err = json.NewDecoder(res.Body).Decode(&got)
	require.NoError(t, err)
	assert.Equal(t, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3QifQ.81IExAPxP110AmC8tgdCYGAAJzjlV00bgPkdVoVSuAk", got.Token)
}

func TestGetRandomeJoke(t *testing.T) {
	jokesRepo := &jokes.RepositoryMock{}
	jokesRepo.FindJokesFunc = func(ctx context.Context, skip, limit int64) ([]storage.Joke, error) {
		return mockJokes, nil
	}
	logger := zap.NewNop()
	config, err := storage.ReadConfig()
	require.NoError(t, err)
	handler := jokes.NewJokesHandler(jokesRepo, nil, &config, logger, 0)
	ts := NewTestServer("/random", handler.RandomJokes, nil)
	defer ts.Close()
	handler.Seed = 0

	request, err := http.NewRequestWithContext(context.Background(), http.MethodGet, ts.URL+"/random?quantity=1", nil)
	require.NoError(t, err)
	client := http.Client{}
	res, err := client.Do(request)
	require.NoError(t, err)
	if res.Body != nil {
		defer res.Body.Close()
	}
	var got []storage.Joke
	err = json.NewDecoder(res.Body).Decode(&got)
	if err != nil {
		require.NoError(t, err)
	}
	assert.Equal(t, mockJokes[0], got[0])
}

func TestAddNewJoke(t *testing.T) {
	jokesRepo := &jokes.RepositoryMock{}
	jokesRepo.FindJokeByIDFunc = func(ctx context.Context, id string) (*storage.Joke, error) {
		return nil, nil
	}
	jokesRepo.InsertJokeFunc = func(ctx context.Context, joke storage.Joke) error {
		return nil
	}
	jokesRepo.FindJokesFunc = func(ctx context.Context, skip, limit int64) ([]storage.Joke, error) {
		return mockJokes, nil
	}
	config, err := storage.ReadConfig()
	require.NoError(t, err)
	logger := zap.NewNop()
	handler := jokes.NewJokesHandler(jokesRepo, nil, &config, logger, 0)

	ts := NewTestServer("/new_joke", handler.RandomJokes, func(h http.Handler) http.Handler {
		return jokes.Middleware(h, handler.Config.TokenKey, logger)
	})
	defer ts.Close()
	newJoke := jokes.MiddlewareRequest{
		Token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VybmFtZSI6InRlc3QyIn0.bxrB2sg-h5e9grAsp-S-thKKle2w9vKOj48TaXD8BgY",
		Body:  "test",
		Title: "test",
		ID:    "testid",
		Score: 0,
	}
	jokeEncoded, err := json.Marshal(newJoke)
	require.NoError(t, err)
	r := bytes.NewReader(jokeEncoded)
	request, err := http.NewRequestWithContext(context.Background(), http.MethodPost, ts.URL+"/new_joke", r)
	require.NoError(t, err)
	client := http.Client{}
	res, err := client.Do(request)
	require.NoError(t, err)
	if res.Body != nil {
		defer res.Body.Close()
	}

	assert.Equal(t, res.StatusCode, 200)
}
