package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"
)

type Joke struct {
	Body  string `json:"body"`
	ID    string `json:"id"`
	Score int    `json:"score"`
	Title string `json:"title"`
}

// JokesSync adding jokes from JSON to MongoDB.
func JokesSync(jokes []Joke, logger *zap.Logger) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Hour)
	defer cancel()
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://admin:jokes@localhost:27017"))
	if err != nil {
		logger.Fatal("Failed to create new client", zap.Error(err))
	}
	if err := client.Connect(ctx); err != nil {
		logger.Fatal("Failed to connect to MongoDB", zap.Error(err))
	}
	database := client.Database("random_jokes")

	jokesCollection := database.Collection("jokes")

	for i, value := range jokes {
		_, err := jokesCollection.InsertOne(ctx, bson.D{
			{Key: "body", Value: value.Body},
			{Key: "title", Value: value.Title},
			{Key: "id", Value: value.ID},
			{Key: "score", Value: value.Score},
		})
		if err != nil {
			logger.Fatal("Failed to insert jokes", zap.Error(err))
		}

		fmt.Println(i, " | ", len(jokes)-1)
	}

	fmt.Println("OK")
}

func parseJSON(logger *zap.Logger) []Joke {
	data, err := ioutil.ReadFile("reddit_jokes.json")
	if err != nil {
		logger.Fatal("Failed to read \"reddit_jokes.json\"", zap.Error(err))
	}

	var jk []Joke

	err = json.Unmarshal(data, &jk)
	if err != nil {
		logger.Fatal("Failed to unmarshal json", zap.Error(err))
	}
	return jk
}

func main() {
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatal(err)
	}
	JokesSync(parseJSON(logger), logger)
}
