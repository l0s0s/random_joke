package storage

// Joke is all jokes look like this.
type Joke struct {
	Body  string `json:"body"`
	ID    string `json:"id"`
	Score int    `json:"score"`
	Title string `json:"title"`
}
