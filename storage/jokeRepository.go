package storage

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"
)

// JokesRepository contain jokes collection.
type JokesRepository struct {
	db *mongo.Collection
}

// NewJokesRepository return repository of jokes.
func NewJokesRepository(db *mongo.Collection) *JokesRepository {
	return &JokesRepository{
		db: db,
	}
}

// InsertJoke insert jokes.
func (repo *JokesRepository) InsertJoke(ctx context.Context, joke Joke) error {
	_, err := repo.db.InsertOne(ctx, bson.D{
		{Key: "body", Value: joke.Body},
		{Key: "title", Value: joke.Title},
		{Key: "id", Value: joke.ID},
		{Key: "score", Value: joke.Score},
	})
	return err
}

// FindJokes return all jokes from BD.
func (repo *JokesRepository) FindJokes(ctx context.Context, skip, limit int64) ([]Joke, error) {
	var cursor *mongo.Cursor

	var err error

	if limit == int64(0) || limit > int64(100) {
		limit = 100
	}

	if skip != 0 {
		cursor, err = repo.db.Find(ctx, bson.D{}, &options.FindOptions{
			Skip:  &skip,
			Limit: &limit,
			Sort: bson.D{
				{Key: "score", Value: -1},
			},
		})
		if err != nil {
			return nil, err
		}
	} else {
		cursor, err = repo.db.Find(ctx, bson.D{}, &options.FindOptions{
			Limit: &limit,
			Sort: bson.D{
				{Key: "score", Value: -1},
			},
		})
		if err != nil {
			return nil, err
		}
	}

	var jokes []Joke

	err = cursor.All(ctx, &jokes)
	if err != nil {
		return nil, err
	}
	if err != nil {
		return nil, err
	}
	return jokes, err
}

// FindJokeByID return all jokes from BD.
func (repo *JokesRepository) FindJokeByID(ctx context.Context, id string) (*Joke, error) {
	var joke *Joke
	result := repo.db.FindOne(ctx, bson.D{
		{Key: "id", Value: id},
	})
	if result != nil {
		err := result.Decode(&joke)
		if err != nil {
			return nil, nil
		}
		return joke, nil
	}
	return nil, nil
}

// IsInJokes is check  is joke with id in database.
func (repo *JokesRepository) IsInJokes(ctx context.Context, id string, logger *zap.Logger) bool {
	joke, err := repo.FindJokeByID(ctx, id)
	if err != nil {
		logger.Error("Failed to find joke", zap.Error(err))
	}
	return joke != nil
}

// CountJokes return length of jokes.
func (repo *JokesRepository) CountJokes(ctx context.Context) (int64, error) {
	count, err := repo.db.CountDocuments(ctx, bson.D{})
	return count, err
}
