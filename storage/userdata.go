package storage

// Userdata is all user information look like.
type Userdata struct {
	Username string `json:"username"`
	Password string `json:"password"`
}
