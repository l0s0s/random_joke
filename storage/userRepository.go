package storage

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
)

// UserRepository contain user collection.
type UserRepository struct {
	db *mongo.Collection
}

// NewUserRepository create new user repo.
func NewUserRepository(db *mongo.Collection) *UserRepository {
	return &UserRepository{
		db: db,
	}
}

// InsertUser one user.
func (repo *UserRepository) InsertUser(ctx context.Context, user Userdata) error {
	_, err := repo.db.InsertOne(ctx, bson.D{
		{Key: "Username", Value: user.Username},
		{Key: "Password", Value: user.Password},
	})
	return err
}

// IsInUsers is check  is user with username in database.
func (repo *UserRepository) IsInUsers(ctx context.Context, username string, logger *zap.Logger) bool {
	user, err := repo.FindUser(ctx, username)
	if err != nil {
		logger.Error("Failed to find user", zap.Error(err))
	}
	return user != nil
}

// FindUser is find user by filter.
func (repo *UserRepository) FindUser(ctx context.Context, username string) (*Userdata, error) {
	var user *Userdata
	result := repo.db.FindOne(ctx, bson.D{
		{Key: "Username", Value: username},
	})
	if result != nil {
		err := result.Decode(&user)
		if err != nil {
			return nil, nil
		}
		return user, nil
	}
	return nil, nil
}
