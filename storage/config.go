package storage

import (
	"time"

	"github.com/caarlos0/env/v6"
)

// Config is what need contains config file.
type Config struct {
	DefaultRequestTimeout time.Duration `env:"REQUESTTIMEOUT"`
	TokenKey              string        `env:"TOKENKEY"`
	MongoURI              string        `env:"MONGOURI"`
}

// ReadConfig is read config file in path.
func ReadConfig() (cfg Config, err error) {
	cfg = Config{}
	err = env.Parse(&cfg)
	return
}
